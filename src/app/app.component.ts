import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Recipe Book';
  selection:string = 'Recipe';

  onNavigate(select: string){
  this.selection = select;
  }
}
