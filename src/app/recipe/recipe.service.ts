import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService{
    recipeSelected = new EventEmitter<Recipe>();
   private recipes: Recipe[] = [
        new Recipe('veg receipe',
         'This is a vegetarian dish', 
        'https://images.pexels.com/photos/958466/pexels-photo-958466.jpeg?cs=srgb&dl=delicious-indian-cooking-indian-cuisine-indian-serving-958466.jpg&fm=jpg',
        [
          new Ingredient('paneer', 1.5),
          new Ingredient('chilli', 8)
        ]),
        new Recipe('chicken receipe',
         'This is a kebab chicken', 
        'https://p2.piqsels.com/preview/88/872/995/chicken-seek-kebab-mugalai-food.jpg',
        [
          new Ingredient('chicken', 2),
          new Ingredient('onion', 4),
        ])
      ];

getRecipes(){
return this.recipes.slice();
}

constructor(private slService: ShoppingListService){}

addIngredientToShoppingList(ingredients: Ingredient[]){
this.slService.addIngredients(ingredients);
}

}