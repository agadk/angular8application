import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  collapsed = true;
  @Output() selection = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit() {
  }

  onSelect(feature: string){
  this.selection.emit(feature);
  }
}
